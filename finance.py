#! /usr/bin/python3

import argparse
import calendar
import csv
import datetime

def main():
    p = argparse.ArgumentParser();
    p.add_argument("table",
                   help="CSV Table to read;\
                   exported from Sparkasse Online Banking")
    p.add_argument("--begin",
                   "-b",
                   help="start of range;\
                   can be either a date in the form of dd.mm.yy\
                   or a search string")
    p.add_argument("--end", "-e", help="end of range;\
                   can be either a date in the form of dd.mm.yy\
                   or a search string")
    p.add_argument("--sum",
                   "-s",
                   action="store_true",
                   help="sum the values")
    p.add_argument("--exclude",
                   "-x",
                   nargs="+",
                   help="exclude all items containing these strings")
    p.add_argument("--include",
                   "-i",
                   nargs="+",
                   help="include all items containing these strings;\
                   using -x or --exclude orverrules this setting")
    p.add_argument("--nopurpose",
                   action="store_true",
                   help="dont show purpose")
    p.add_argument("--noexclude",
                   action="store_true",
                   help="dont show excluded")
    p.add_argument("--width",
                   "-w",
                   help="row width to display")
    args = p.parse_args()
    start, end = datetime.datetime.min, datetime.datetime.now()
    filter_begin = False
    if args.begin:
        try:
            start = datetime.datetime.strptime(args.begin, "%d.%m.%y")
        except ValueError:
            filter_begin = True

    filter_end = False
    if args.end:
        try:
            end = datetime.datetime.strptime(args.end, "%d.%m.%y")
        except ValueError:
            filter_end = True

    table = []
    with open(args.table, encoding="latin-1") as file:
        table = list(csv.reader(file, delimiter=";"))

    total = 0.0
    in_range = False
    range_finished = False
    width = int(args.width) if args.width else 241

    w = width - 21
    name_width = int(w * (0.2)) if not args.nopurpose else w - 1
    purpose_width = 1 if args.nopurpose else w - name_width

    for row in table[1:]:
        (_, booking_day, valuta,
         booking_text, purpose, name,
         _, _, value,
         _, _) = row
        booking_date = datetime.datetime.strptime(booking_day, "%d.%m.%y")
        if filter_end:
            in_range = (in_range or args.end in name
                        or args.end in purpose)
        else:
            in_range = ((end - booking_date).total_seconds() > 0
                        and not range_finished)
        if filter_begin:
            in_range = (in_range and not (args.begin in name
                        or args.begin in purpose))
            range_finished = (range_finished or args.begin in name
                              or args.begin in purpose)
        else:
            in_range = in_range and (booking_date - start).total_seconds() > 0

        filtered = False
        if args.exclude:
            filtered = any([x in name or x in purpose for x in args.exclude])
        elif args.include:
            filtered = not (any([(i in name
                                  or i in purpose) for i in args.include]))

        if in_range:
            if args.noexclude and filtered:
                continue

            op = "" if not filtered else "("
            cl = "" if not filtered else ")"
            value = float(value.replace(",", "."))

            name = name[:min(name_width - 3, len(name))]
            purpose = purpose[:max(min(purpose_width - 3, len(purpose)), 0)]

            purpose_str = "(:.<{})".format(purpose_width)
            purpose = "" if args.nopurpose else purpose

            format_str = "() (:.<{}) {} (:1)(:8.2f)(:1)"
            format_str = format_str.format(name_width, purpose_str)
            format_str = format_str.replace("(", "{").replace(")", "}")
            
            print(format_str
                  .format(booking_day, name, purpose, op, value, cl))
            if args.sum and not filtered:
                total += value
    if args.sum:
        print("_"*width)
        tot = "Total: "
        tot = "\n{}(:{}.2f)".format(tot, width - len(tot)).replace("(", "{").replace(")", "}")
        print(tot.format(total))

if __name__ == "__main__":
    main()
